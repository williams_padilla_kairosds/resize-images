import { response } from "../../application/task.response";
import { Image } from "../../domain/entities/image.entity";
import { Task } from "../../domain/entities/task.entity";
import { TaskModel } from "./task.model";

export const taskRepository = {
  createTask: async (task: Task, image: Image) => {
    const id = task.id;
    const status = task.status;

    const taskModel: any = await TaskModel.create({
      id,
      status,
    });

    const md5 = image.md5;
    const path = image.tempFilePath;
    const imageData = image.data;
    const imageType = image.mimetype;
    const imageName = image.name;
    const height = image.height;
    const width = image.width;

    await taskModel.createImage({
      id: image.id,
      md5,
      path,
      imageData,
      imageType,
      imageName,
      height,
      width,
    });

    return taskModel;
  },
  getStatus: async (taskId: string) => {
    const taskModel: any = await TaskModel.findOne({ where: { id: taskId } });

    const { message } = response.TASK_NOT_FOUND;
    if (!taskModel) throw new Error(message);
    return taskModel;
  },
};
