import { response } from "../../application/task.response";
import { TaskModel } from "./task.model";

export const imageRepository = {
  saveImage: async (taskId: string, image: any) => {
    const taskModel: any = await TaskModel.findOne({ where: { id: taskId } });

    const { message } = response.TASK_NOT_FOUND;
    if (!taskModel) throw new Error(message);

    const md5 = image.md5;
    const path = image.tempFilePath;
    const imageData = image.data;
    const imageType = image.mimetype;
    const imageName = image.name;
    const height = image.height;
    const width = image.width;

    await taskModel.createImage({
      id: image.id,
      md5,
      path,
      imageData,
      imageType,
      imageName,
      height,
      width,
    });

    return await TaskModel.update(
      { status: "DONE" },
      { where: { id: taskId } }
    );
  },
};
