import { DataTypes } from "sequelize";
import sequelize from "../config/postgresql";
import { ImageModel } from "./image.model";

const TaskModel = sequelize.define("tasks", {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true,
  },
  status: {
    type: DataTypes.ENUM("PENDING", "DONE"),
    allowNull: false,
  },
});

TaskModel.hasMany(ImageModel);
ImageModel.belongsTo(TaskModel, {
  foreignKey: {
    allowNull: false,
  },
});
console.log(
  "------------------> Task - Images relation created <-----------------"
);

export { TaskModel };
