import { Request, Response } from "express";
import { response } from "../../application/task.response";
import { imageDomain } from "../../domain/image.domain";
import { taskDomain } from "../../domain/task.domain";

export const controller = {
  createTask: async (req: Request, res: Response) => {
    let result: any;
    const { files: image } = req;
    const { message, status } = response.createTaskOK;

    try {
      const task = await taskDomain.createTask(image);

      result = {
        status,
        body: {
          message,
          task,
        },
      };
    } catch (e: any) {
      result = {
        status: 400,
        body: {
          message: e.message,
        },
      };
    }
    res.on("finish", () => {
      if (result.body.task)
        imageDomain.saveAndResizeImageToDB(image, result.body.task.id);
    });

    return res.status(result.status).send(result.body);
  },
  getTaskStatus: async (req: Request, res: Response) => {
    let result;
    const {
      params: { taskId },
    } = req;
    const { message, status } = response.getStatusOK;
    try {
      const task = await taskDomain.getStatus(taskId);

      result = {
        status,
        body: {
          message,
          task,
        },
      };
    } catch (e: any) {
      result = {
        status: 400,
        body: {
          message: e.message,
        },
      };
    }
    return res.status(result.status).send(result.body);
  },
};
