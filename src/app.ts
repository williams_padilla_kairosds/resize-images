import bodyParser from "body-parser";
import express from "express";
import fileUpload from "express-fileupload";
import { controller } from "./infrastructure/controller/task.controller";

const app = express();
app.use(fileUpload());
app.use(bodyParser.json());

app.post("/task", controller.createTask);
app.get("/task/:taskId", controller.getTaskStatus);

app.listen(3000, () => {
  console.log("server starter");
});
