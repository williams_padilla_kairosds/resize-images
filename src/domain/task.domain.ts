import { v4 as uuidv4 } from "uuid";
import { taskRepository } from "../infrastructure/repositories/task.repository";
import { Task } from "./entities/task.entity";
import { setInfo } from "./services/image-info.service";

export const taskDomain = {
  createTask: async (file: any) => {
    if (!file) throw new Error("Not file upload");
    const id = uuidv4();
    const task: Task = {
      id,
      status: "PENDING",
    };
    const image = setInfo(file);
    const response = await taskRepository.createTask(task, image);
    return response;
  },
  getStatus: async (taskId: string) => {
    const response = await taskRepository.getStatus(taskId);
    return response;
  },
};
