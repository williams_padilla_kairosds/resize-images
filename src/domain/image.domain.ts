import { imageRepository } from "../infrastructure/repositories/image.repository.pg";
import { getResizedImages } from "./services/image-resize-cloud.service";
import { uploadResizedImages } from "./services/upload-resized-image.service";

export const imageDomain = {
  saveAndResizeImageToDB: async (file: any, taskId: string) => {
    const { content } = await getResizedImages(file);
    const images = uploadResizedImages(file, content);

    images.forEach(async (image) => {
      await imageRepository.saveImage(taskId, image);
    });
  },
};
