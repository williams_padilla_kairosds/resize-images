export interface Image {
  id: string;
  md5: string;
  tempFilePath: string;
  data: Blob;
  mimetype: string;
  name: string;
  height: number | undefined;
  width: number | undefined;
}
