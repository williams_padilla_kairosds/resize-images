import sizeOf from "image-size";
import { v4 as uuidv4 } from "uuid";
import { Image } from "../entities/image.entity";

export const setInfo = (file: any): Image => {
  const { image } = file;
  const { height, width } = sizeOf(image.data);

  const id = uuidv4();
  const md5 = image.md5;
  const tempFilePath = image.tempFilePath;
  const data = image.data;
  const mimetype = image.mimetype;
  const name = image.name;

  return {
    id,
    md5,
    tempFilePath,
    data,
    mimetype,
    name,
    height,
    width,
  };
};
