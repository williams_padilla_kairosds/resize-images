import axios from "axios";
import { createHash } from "crypto";
// import FormData from "form-data";
import * as fs from "fs-extra";
import { join } from "path";
import sharp from "sharp";

export const getResizedImages = async (file: any) => {
  const { image } = file;
  const filePath = image.name;
  const fileName = filePath.split(".")[0];
  const fileExtension = filePath.split(".")[1];
  const blob = new Blob([image.data]);

  const form = new FormData();
  form.append("file", blob, image.name);
  try {
    const { data } = await axios.post(
      "https://us-central1-kds-pruebastec.cloudfunctions.net/resize-wpf-function",
      form
    );

    //saving files to the output directory
    const projectPath = __dirname.split("/").slice(0, -3).join("/");
    const savedFiles = data.content.map(async (resizedImg: any) => {
      const size = parseInt(Object.keys(resizedImg)[0].split("_")[1]);
      const bufferPath = resizedImg[`file_${size}`];
      const buffer = Buffer.from(bufferPath);
      const outputDir = join(projectPath, "output", fileName, `${size}`);

      await fs.ensureDir(outputDir);

      const hash = createHash("md5").update(buffer).digest("hex");
      const hashedFile = join(outputDir, `${hash}.${fileExtension}`);
      await sharp(buffer).toFile(hashedFile);
    });
    await Promise.all(savedFiles);
    return data;
  } catch (e) {
    console.log(e);
  }
};
