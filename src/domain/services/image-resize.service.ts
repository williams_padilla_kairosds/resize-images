import { createHash } from "crypto";
import * as fs from "fs-extra";
import { tmpdir } from "os";
import { join } from "path";
import sharp from "sharp";

export const resizeImage = async (file: any) => {
  const { image } = file;
  const filePath = image.name;
  const fileName = filePath.split(".")[0];
  const fileExtension = filePath.split(".")[1];
  const workingDir = join(tmpdir(), "original");

  let tmpPathFile: string;

  const projectPath = __dirname.split("/").slice(0, -3).join("/");

  await fs.ensureDir(workingDir);
  tmpPathFile = join(workingDir, filePath);

  const sizes = [800, 1024];

  const uploadPromises = sizes.map(async (size: number) => {
    const outputDir = join(projectPath, "output", fileName, `${size}`);
    await image.mv(tmpPathFile);
    await fs.ensureDir(outputDir);

    const tempDir = join(tmpdir(), "output");
    await fs.ensureDir(tempDir);
    const tempOutputFile = join(tempDir, `output_${size}.${fileExtension}`);
    await sharp(tmpPathFile).resize(size).toFile(tempOutputFile);

    const buff = fs.readFileSync(tempOutputFile);
    const hash = createHash("md5").update(buff).digest("hex");

    const hashedFile = join(outputDir, `${hash}.${fileExtension}`);
    await sharp(tempOutputFile).toFile(hashedFile);
    await fs.remove(tempDir);
  });

  await Promise.all(uploadPromises);

  return await fs.remove(workingDir);
};
