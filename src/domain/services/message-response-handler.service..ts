export const createInternalErrorResponse = (
  message: string,
  status: number
) => ({
  message,
  status,
});

export const createInternalResponse = (message: string, status: number) => ({
  message,
  status,
});
