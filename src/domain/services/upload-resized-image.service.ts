import { createHash } from "crypto";
import sizeOf from "image-size";
import { v4 as uuidv4 } from "uuid";
import { Image } from "../entities/image.entity";

export const uploadResizedImages = (originFile: any, images: any): Image[] => {
  const { image } = originFile;

  const imagesToUpload = images.map((resizedImg: any) => {
    const size = parseInt(Object.keys(resizedImg)[0].split("_")[1]);
    const bufferPath = resizedImg[`file_${size}`];
    const buffer = Buffer.from(bufferPath);
    const { height, width } = sizeOf(buffer);

    const md5 = createHash("md5").update(buffer).digest("hex");

    return {
      id: uuidv4(),
      md5,
      tempFilePath: "",
      data: buffer,
      mimetype: image.mimetype,
      name: `size@${size}_${image.name}`,
      height,
      width,
    };
  });
  return imagesToUpload;
};
