import {
  createInternalErrorResponse,
  createInternalResponse,
} from "../domain/services/message-response-handler.service.";

export const response = {
  createTaskOK: createInternalResponse(
    "Task have been created succeffully",
    202
  ),
  getStatusOK: createInternalResponse("Task status OK", 200),
  imageUpload_KO: createInternalErrorResponse("Image file is missing", 400),
  TASK_NOT_FOUND: createInternalErrorResponse("Task not found", 400),
};
