const path = require("path");
const os = require("os");
const fs = require("fs");
const sharp = require("sharp");
const express = require("express");

const Busboy = require("busboy");

const app = express();
app.use(express.json());

app.post("/", (req, res) => {
  if (req.method !== "POST") {
    return res.status(405).end();
  }
  const busboy = Busboy({ headers: req.headers });
  const tmpdir = os.tmpdir();

  const uploads = {};

  const fileWrites = [];

  busboy.on("file", (fieldname, file, { filename }) => {
    const filepath = path.join(tmpdir, filename);
    uploads[fieldname] = filepath;

    const writeStream = fs.createWriteStream(filepath);
    file.pipe(writeStream);

    const promise = new Promise((resolve, reject) => {
      file.on("end", () => {
        writeStream.end();
      });
      writeStream.on("close", resolve);
      writeStream.on("error", reject);
    });
    fileWrites.push(promise);
  });

  const resizedImages = [];

  busboy.on("finish", async () => {
    await Promise.all(fileWrites);

    for (const file in uploads) {
      const sizes = [800, 1024];
      for (const size of sizes) {
        const resized = await sharp(uploads[file]).resize(size).toBuffer();
        resizedImages.push({
          [`${file}_${size}`]: resized,
        });
      }
      fs.unlinkSync(uploads[file]);
    }

    res.status(202).send({ message: "Processed", content: resizedImages });
  });

  busboy.end(req.rawBody);
});

exports.resizeFunction = app;
