## Frameworks used

- image-size - to get image dimension
- Sequelize(Postgres) for models, relation and connection
- fs, crypto - default node modules
- sharp - resize images
- uuid - to create unique ids
- Axios - for HTTP request
- nodemon - watch changes to restart compilation
- Typescript

## Prerequisites

- Install node18
- Install gCloud CLI Client and google-cloud/functions-framework (if tested locally)
- Docker & Docker-compose
- Postman

## Steps to start server

- Clone the repository
- In the server directory, start the docker container

```bash
    docker-compose up --build -d
```

- Install dependencies

```bash
    npm i
```

- Start the server

```bash
    npm run start
```

- To deploy gCloud functions locally, go to `/resize_function` then

```bash
    npm run start
```

it will deploy gCloud function locally at `http://localhost:8080/`

## Steps for requests

- Open postman and import the collection from postman folder, and it's ready to make request
